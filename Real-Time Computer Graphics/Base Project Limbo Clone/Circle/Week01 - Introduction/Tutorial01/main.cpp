#include <FreeImage\FreeImagePlus.h>
#include <wincodec.h>
#include <glew\glew.h>
#include <freeglut\freeglut.h>
#include <CoreStructures\CoreStructures.h>
#include <iostream>
#include "texture_loader.h"

using namespace std;
using namespace CoreStructures;



// Variable we'll use to animate (rotate) our object
float theta = 0.0f;

// Variables needed to track where the mouse pointer is so we can determine which direction it's moving in
int mouse_x, mouse_y;
bool mDown = false;

// Function prototypes
void init(int argc, char* argv[]);
void display(void);
void hexagon(void);

void mouseButtonDown(int button_id, int state, int x, int y);
void mouseMove(int x, int y);
void keyDown(unsigned char key, int x, int y);



int main(int argc, char* argv[]) {

	cout << "Hello, World!\n";

	init(argc, argv);

	glutMainLoop();

	return 0;
}


void init(int argc, char* argv[]) {

	// 1. Initialise FreeGLUT
	glutInit(&argc, argv);

	glutInitContextVersion(4, 3);
	glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE);

	glutInitWindowSize(800, 800);
	glutInitWindowPosition(64, 64);
	glutCreateWindow("Hello OpenGL!");

	// Display callback
	glutDisplayFunc(display);

	glutKeyboardFunc(keyDown);
	glutMouseFunc(mouseButtonDown); // mouse button handler
	glutMotionFunc(mouseMove); // mouse move handler


	// 2. Initialise GLEW library
	GLenum err = glewInit();

	// Ensure the GLEW library was initialised successfully before proceeding
	if (err == GLEW_OK) {

		cout << "GLEW initialised okay\n";
	}
	else {

		cout << "GLEW could not be initialised!\n";
		throw;
	}


	// 3. Initialise OpenGL settings and objects we'll use in our scene
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	//glMatrixMode(GL_PROJECTION);
	//gluOrtho2D(-10.0, 10.0, -10.0, 10.0);
}


void display(void) {

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	GUMatrix4 R = GUMatrix4::rotationMatrix(0.0, 0.0f, theta);
	glLoadMatrixf((GLfloat*)&R);

	glBegin(GL_TRIANGLE_FAN);



	/*glColor3ub(0, 255, 0);
	glVertex2f(0.0f, 0.0f);

	glColor3ub(255, 0, 0);
	glVertex2f(-0.3f, -0.2f);

	glColor3ub(0, 0, 255);
	glVertex2f(-0.2f, 0.1f);

	glColor3ub(0, 0, 255);
	glVertex2f(-0.5f, 0.3f);

	glColor3ub(0, 0, 255);
	glVertex2f(-0.1f, 0.35f);

	glColor3ub(255, 0, 0);
	glVertex2f(0.0f, 0.7f);

	glColor3ub(255, 0, 0);
	glVertex2f(0.1f, 0.35f);

	glColor3ub(255, 0, 0);
	glVertex2f(0.5f, 0.3f);

	glColor3ub(0, 255, 0);
	glVertex2f(0.2f, 0.1f);

	glColor3ub(0, 255, 0);
	glVertex2f(0.3f, -0.2f);*/

	glColor3ub(255, 0, 0);
	glVertex2f(0.0f, 0.0f);

	glColor3ub(255, 0, 0);
	glVertex2f(1.0f, 0.0f);

	glColor3ub(0, 255, 0);
	glVertex2f(0.965917f, 0.258852f);

	glColor3ub(0, 0, 255);
	glVertex2f(0.865991f, 0.500059f);

	glColor3ub(255, 0, 0);
	glVertex2f(0.707035f, 0.707179f);

	glColor3ub(0, 255, 0);
	glVertex2f(0.499882f, 0.866093f);

	glColor3ub(0, 0, 255);
	glVertex2f(0.258655f, 0.96597f);

	glColor3ub(255, 0, 0);
	glVertex2f(-0.000203653f, 1.0f);

	glColor3ub(0, 255, 0);
	glVertex2f(-0.259049f, 0.965864f);

	glColor3ub(0, 0, 255);
	glVertex2f(-0.500235f, 0.86589f);

	glColor3ub(255, 0, 0);
	glVertex2f(-0.707323f, 0.706891f);

	glColor3ub(0, 255, 0);
	glVertex2f(-0.866195f, 0.499706f);

	glColor3ub(0, 0, 255);
	glVertex2f(-0.966022f, 0.258458f);

	glColor3ub(255, 0, 0);
	glVertex2f(-1.0f, -0.000407783f);

	glColor3ub(0, 255, 0);
	glVertex2f(-0.965811f, -0.259246f);

	glColor3ub(0, 0, 255);
	glVertex2f(-0.865787f, -0.500412f);

	glColor3ub(255, 0, 0);
	glVertex2f(-0.706746f, -0.707467f);

	glColor3ub(0, 255, 0);
	glVertex2f(-0.499529f, -0.866297f);

	glColor3ub(0, 0, 255);
	glVertex2f(-0.258261f, -0.966075f);

	glColor3ub(255, 0, 0);
	glVertex2f(0.000611317f, -1.0f);

	glColor3ub(0, 255, 0);
	glVertex2f(0.259442f, -0.965759f);

	glColor3ub(0, 0, 255);
	glVertex2f(0.500588f, -0.865686f);

	glColor3ub(255, 0, 0);
	glVertex2f(0.707611f, -0.706603f);

	glColor3ub(0, 255, 0);
	glVertex2f(0.866398f, -0.499353f);

	glColor3ub(0, 0, 255);
	glVertex2f(0.966128f, -0.258065f);

	glColor3ub(255, 0, 0);
	glVertex2f(1.0f, 0.0f);

	glEnd();

	glutSwapBuffers();
}


void hexagon(void) {

	float dTheta = 3.142f * 2.0f / 6.0f;

	glBegin(GL_TRIANGLE_FAN);

	glVertex2f(0.0f, 0.0f);

	glVertex2f(1.0f, 0.0f);

	glVertex2f(cos(dTheta * 1.0f), sin(dTheta * 1.0f));
	glVertex2f(cos(dTheta * 2.0f), sin(dTheta * 2.0f));
	glVertex2f(cos(dTheta * 3.0f), sin(dTheta * 3.0f));
	glVertex2f(cos(dTheta * 4.0f), sin(dTheta * 4.0f));
	glVertex2f(cos(dTheta * 5.0f), sin(dTheta * 5.0f));
	glVertex2f(cos(dTheta * 6.0f), sin(dTheta * 6.0f));

	glEnd();
}

#pragma region Keyboard and mouse functions

void mouseButtonDown(int button_id, int state, int x, int y) {

	if (button_id == GLUT_LEFT_BUTTON) {

		if (state == GLUT_DOWN) {

			cout << "mouse down!\n";

			mouse_x = x;
			mouse_y = y;

			mDown = true;

		}
		else if (state == GLUT_UP) {

			cout << "mouse up!\n";

			mDown = false;
		}
	}
}

void mouseMove(int x, int y) {

	if (mDown) {

		int dx = x - mouse_x;
		int dy = y - mouse_y;

		theta += (float)dy * (3.142f * 0.01f);

		mouse_x = x;
		mouse_y = y;

		glutPostRedisplay();
	}
}

void keyDown(unsigned char key, int x, int y) {

	if (key == 'r') {

		theta = 0.0f;
		glutPostRedisplay();
	}
}

#pragma endregion
